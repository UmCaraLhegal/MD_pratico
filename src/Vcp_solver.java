//******************************************************************************
//******************************************************************************
//  (Vcp_solver)                                                              
//******************************************************************************
//  Autores                                                                   
//  Fellipe Eduardo Peixto da Rocha, numero de matricula: 15276                      
//  Vitor Fernandes, numero de matricula:                                            
//******************************************************************************
//  (Instrucoes de compilacao)                                                
//  VERIFIQUE A VERSAO DA SUA JVM!!!
//  INFO (utilizado nesse trabalho)
//  java version "1.8.0_111"//
//  Java(TM) SE Runtime Environment (build 1.8.0_111-b14)
//  Java HotSpot(TM) 64-Bit Server VM (build 25.111-b14, mixed mode)
//  
//******************************************************************************
//  Desenvolvido na IDE NetBeans 8.2                                          
//******************************************************************************
//  data: 22/11/2016                                                          
//  Essa classe e a principal, aqui sao chamadas as classes colaboradoras.    
//  Os parametros sao repassados por linha de comando.                        
//******************************************************************************
//  (cabecalho funcoes auxliares)                                             
//  (procedimentos implementados)                                             
//******************************************************************************
//******************************************************************************

import Arquivo.*;
import java.io.IOException;

public class Vcp_solver {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        
        //semente geradora de numeros aleatorios, informado por linha de comando
        int semente = Integer.valueOf(args[0]);
        
        //respectivo caracter informado por linha de comando 
        //que indica metodo a ser utilizado
        // a - para metodo aproximado, g - para metodo guloso
        char metodo = args[1].charAt(0);
        
        //nome do arquivo de entrada informado por linha de comando
        String ArqEntrada = args[2];
        
        //nome do arquivo de saida, informado por linha de comando
        String ArqSaida = args[3];
        
        //escrita do arquivo 
        EscritaArquivo arqSaida = new EscritaArquivo(ArqSaida);
        arqSaida.escrever(ArqEntrada, "QUANTIDADE DE NOS!", 
                "QUANTIDADE DE ARCOS!",Integer.toString(semente), metodo, 
                "COBERTURA!");
        
        System.out.println("Vertex\tCover\tSolver");
        System.out.println("======\t======\t======");
        System.out.println();
        System.out.println("Nodes:\t(NOS)");
        System.out.println("Arcs:\t(ARCOS)");
        System.out.println();
        System.out.println("Seed:\t"+semente);
        if (metodo=='g')
        {
            System.out.println("Method:\tGreedy");
        }else{
            System.out.println("Method:\tAproximado");
        }
        System.out.println("Input:\t"+ArqEntrada);
        System.out.println("Output:\t"+ArqSaida);
        System.out.println();
        System.out.println("Cover:\t(COBERTURA)");
        System.out.println();
        System.out.println("End of Processing");
    }
    
}